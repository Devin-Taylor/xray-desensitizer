# Use phusion/baseimage as base image. To make your builds
# reproducible, make sure you lock down to a specific version, not
# to `latest`! See
# https://github.com/phusion/baseimage-docker/blob/master/Changelog.md
# for a list of version numbers.
FROM devintaylor/desensitizer-tool

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# environment variables
ENV datapath /host
ENV writepath /host/Desktop

# ...put your own build instructions here...
# Update aptitude with new repo
RUN apt-get update
# Install software
RUN apt-get install -y git
# Make ssh dir
RUN mkdir /root/.ssh/
# Copy over private key, and set permissions
# id_rsa must be in the same directory
ADD id_rsa /root/.ssh/id_rsa
# Create known_hosts
RUN touch /root/.ssh/known_hosts
# Add gitlab keys
RUN ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
# Clone the conf files into the docker container
# NOTE: Insert you Gitlab username here
CMD git clone git@gitlab.com:<user-name>/xray-desensitizer.git && cd xray-desensitizer/src && python3 clean_xrays.py --inpath=${datapath} --outpath=${writepath}

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
